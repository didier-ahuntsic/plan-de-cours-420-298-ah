#let section-counter = counter("section-counter")

#let section(content) = {
    section-counter.step()
    let idx = section-counter.display();
    heading(level: 3)[ Partie #idx: #content ];
}
  

#let deroulement-du-cours = [


#section()[Preparer l'environnement et introduction au web]

==== Objectifs spécifiques:

À l'issue de cette étape, l'étudiant capable de préparer l'environnement 
de développement informatique et sera familier avec la terminologie du
web et des bases du langage JavaScript.

==== Contenu

Préparation de l'environnement de développement informatique

- Installation d'un environnement de développement "intégré" (IDE) 
  pour JavaScript, WebStorm
- Installation de Node (aucune référence à son fonctionnement interne),
  NPM (Node Package Manager) et TypeScript


Élements de base du Web

- html 
- css 
- javascript 

Éléments de base du langage JavaScript

- Structure d'un script
- Hello world


==== Méthodologie

Une présentation des concepts théoriques et des démonstrations 
pratiques par l'enseignant permettra à l'élève de réaliser les 
activités d'apprentissage.


==== Activités d'apprentissage

Exercices à faire en classe et à compléter à la maison.


#section()[Élements de base du langage javascript]

==== Objectifs spécifiques


À l'issue de cette étape, l'élève sera capable de concevoir des 
programmes utilisant des variables, des opérateurs, des instructions
sur les fonctions, tableaux et objets.


==== Contenu


Éléments de base du langage JavaScript
- Variables et constantes
- Opérateurs arithmétiques, comparaison et logiques
- Chaînes de caractères
- Structures conditionnelles
- Boucles
- Fonctions
- Tableaux et méthodes
- Introduction à la POO

==== Méthodologie

Une présentation des concepts théoriques et des démonstrations pratiques par l'enseignant permettra à l'élève de réaliser les activités d'apprentissage.


==== Activités d'apprentissage

Exercices à faire en classe et à compléter à la maison.



#section()[Javascript et web]

==== Objectifs spécifiques

À l'issue de cette étape, l'élève sera capable de concevoir
des programmes javascript utilisant des instructions intéragissant avec
le reste de la page web.

==== Contenu

- Formulaires
- Validation
- Introduction au DOM ;
- Gestion des évènements

==== Méthodologie

Une présentation des concepts théoriques et des démonstrations pratiques 
par l'enseignant permettra à l'élève de réaliser les activités 
d'apprentissage.


==== Activités d'apprentissage

Exercices à faire en classe et à compléter à la maison.




]
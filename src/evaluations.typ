#let evaluations = [

== Évaluation

=== Évaluations formatives
À l'occasion, l'enseignant fournira des exercices à faire en classe 
ou des devoirs à compléter à la maison qui permettront à l'élève de 
vérifier sa compréhension de la nouvelle matière présentée et de se 
familiariser avec le type de questions auxquelles il doit être en 
mesure de répondre.


=== Évaluations Sommatives


#table(
  columns: (auto, 1fr, auto),
  inset: 10pt,
  align: horizon,
  [Travaille Pratique 1], [cours 5, remise au cours 9], [30%], 
  [Travaille Pratique 2], [cours 10, remise au cours 14], [35%], 
  [Travaille Pratique 3], [cours 15, remise au cours 19], [35%],
)

==== Détails

- Tous les travaux comportent une partie orale individuelle où l'étudiant 
  doit expliquer les choix pris lors du travail.  Cette partie orale 
  visera à évaluer la compréhension du travail remis par chaque 
  membre de l'équipe. 

- Tout travail est noté individuellement.  Si un travail peut être 
  effectué en équipe, les notes sont données indépendamment à 
  chaque membre  de l'équipe.

- L'enseignant se réserve le droit changer les équipes à n'importe quel
  moment s'il a l'impression que la charge de travaille est distribué
  inéquitablement parmi les membres de l'équipe.

- La remise de travail en retard est inacceptable.  La première journée 
  de retard entrainera une pénalité de 20%, les journées de retard 
  subséquentes entraineront une pénalité de 10%.


==== Notes relatives aux évaluations

- La note de passage est de 60%.
- Les travaux doivent être remis à l'échéance fixée.  La note zéro 
  sera attribuée au travaux remis en retard.
- En cas de plagiat, la note zéro sera attribuée pour l'évaluation 
  concernée.
- Vous avez la responsabilité de conserver vos évaluations et vous 
  devrez présenter celles-ci lors d'une demande de révision de note.

- L'usage de Chat GPT ou technologie similaire est permise comme 
  outil de référence.  Copier des réponses directement de chat GPT
  (ou autre) est considéré comme un plagit.

]

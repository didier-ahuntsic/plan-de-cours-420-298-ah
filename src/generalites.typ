#let generalites = [
== Présentation générale

Ce cours du 1er bloc de formation est le premier de deux cours 
portant sur le développement d'applications Web côté client. Il 
prépare donc l'étudiant aux apprentissages réalisés dans le deuxième 
cours de programmation Web côté client qui lui fait suite.


À l'issue de ce cours, l'étudiant sera en mesure de développer
une application Web côté client en utilisant le langage de
programmation JavaScript.

Les objectifs intermédiaires de ce cours sont de créer un 
formulaire en HTML (HyperText Markup Language), utiliser des 
feuilles de style en cascade (CSS) et programmer une application 
Web côté client à l'aide du langage JavaScript.


Les principaux thèmes abordés dans ce cours sont : le langage de programmation côté client JavaScript et TypeScript; les interactions avec l'usager.



]
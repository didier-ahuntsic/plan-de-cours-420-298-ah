#let _ponderation = link("https://www.cegepsquebec.ca/nos-cegeps/presentation/systeme-scolaire-quebecois/grille-de-cours-et-ponderation/")[Pondération]

#let frontpage = [ 
#align(center)[
    #image("logo-ahuntsic.png", width: 33%),

    #text(size: 20pt)[Plan De Cours]

    #text(size: 12pt)[Automn 2023]
]

#table(
  columns: (auto, 1fr),
  inset: 25pt,
  align: horizon,
  [Titre du cours],[*Programmation Web côté client I*],
  [Code],[*420-298-AH*],
  _ponderation, text(weight: "bold", font: "FreeMono")[2-3-2],
  [Compétences visées], list(
        [*AF56 - Utiliser un langage de programmation.*],
    ),
  [Préalable], [Aucun],
  [Corequis], [
    - 581-449-AH, Éléments d'infographie pour sites Web
  ],
  [Enseignant], [
    - *Didier Amyot*
    - #link("mailto:didier.amyot@collegeahuntsic.qc.ca")[didier.amyot\@collegeahuntsic.qc.ca]
    ],
  [Département], [*Informatique*]
)
]

